/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smarttmt.preuba_reactivo.notifica;


import java.awt.print.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


/**
 *
 * @author brandon
 */
@Repository
public interface NotificaRepository extends ReactiveMongoRepository<Notifica, String>{

    /**
     * Funcion para traer todas las notificaciones
     * @param typenoti
     * @return 
     */
    @Tailable
    public Flux<Notifica> findByTypenoti(String typenoti);
    
    @Query("{ id: { $exists: true }}")
    Flux<Notifica> retrieveAllQuotesPaged();
    
    @Tailable
    //@Query(value = "{ 'typenoti' : ?0 }")
    Flux<Notifica> findDistinctByEventname(String eventname);
    
    @Tailable
    Flux<Notifica> findWithTailableCursorBy();

    

}
