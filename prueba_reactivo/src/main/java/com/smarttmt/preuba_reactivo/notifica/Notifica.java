package com.smarttmt.preuba_reactivo.notifica;



import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mongodb.lang.NonNull;
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;


@ToString
@Getter
@Setter
@Document(collection = "notifica")
//@ApiModel(description = "Este API se usa gestionar las notificaciones del usuario")
public class Notifica implements Serializable {
    
    @Autowired(required=true)
    private NotificaRepository notificaRepository;

    @Id
//    @ApiModelProperty(required = false)
    private String id;
    
    @NonNull
//    @ApiModelProperty(required = true)
    private String eventname;
    
    @NonNull
//    @ApiModelProperty(required = true)
    private String eventdesc;
    
    //@JsonIgnore
    private String typenoti;


}