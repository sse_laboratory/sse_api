/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smarttmt.preuba_reactivo.notifica;

import java.sql.Date;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import static java.util.Arrays.stream;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.validation.Valid;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author brandon
 */
@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping("/")
public class NotificaController {

    @Autowired(required = true)
    private NotificaRepository notificaRepository;

    @GetMapping(value = "/welcome")
    public String saveAndSend() {
        return "Hola mundo";
    }

    @GetMapping(value = "/save")
    public Mono<Notifica> saveAndSend(@Valid Notifica notifica) {
        //@Valid Notifica notifica

        Mono<Notifica> notificaMono
                = notificaRepository.save(notifica);
        return notificaMono;

        //return new ResponseEntity(emitter, HttpStatus.OK);
    }

    @GetMapping(value = "/list",
            //            produces = MediaType.TEXT_EVENT_STREAM_VALUE)
            produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Notifica> list() {
        //@Valid Notifica notifica

        Flux<Notifica> f = notificaRepository.findAll();
        return f;
        //return new ResponseEntity(emitter, HttpStatus.OK);
    }

    @GetMapping(value = "/list2",
            produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @ResponseBody
    public Flux<Notifica> list2() {

        return notificaRepository.findWithTailableCursorBy().delayElements(Duration.ofMillis(2500));
    }

    @GetMapping(value = "/listEven",
            produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Notifica> listEven() {
        return notificaRepository.findByTypenoti("n").delayElements(Duration.ofMillis(2500));
    }

    @GetMapping(path = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<ServerSentEvent<Notifica>> streamEvents() {

        return notificaRepository.findWithTailableCursorBy()
                .delayElements(Duration.ofMillis(2500))
                .map(sequence -> ServerSentEvent.<Notifica>builder()
                .id(String.valueOf(sequence.getId()))
                .event("ping")
                .data(sequence)
                .build());
    }

    @GetMapping(path = "/stream-flux", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<ServerSentEvent<Notifica>> streamFlux() {

        return notificaRepository.findWithTailableCursorBy()
                .delayElements(Duration.ofMillis(1000))
                .map(sequence -> ServerSentEvent.<Notifica>builder()
                .id(String.valueOf(sequence.getId()))
                .event("ping")
                .data(sequence)
                .build());
    }

    

    @GetMapping(path = "/stream-flux2", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<ServerSentEvent<Notifica>> streamFlux2(@Valid @RequestParam String evento) {

        return notificaRepository.findDistinctByEventname(evento)
                .delayElements(Duration.ofMillis(1000))
                .map(sequence -> {
                    return ServerSentEvent.<Notifica>builder()
                            .id(String.valueOf(sequence.getId()))
                            .event(evento)
                            .data(sequence)
                            .build();
                });

    }

}
