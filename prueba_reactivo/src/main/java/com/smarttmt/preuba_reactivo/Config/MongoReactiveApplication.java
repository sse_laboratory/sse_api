/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smarttmt.preuba_reactivo.Config;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.smarttmt.preuba_reactivo.notifica.NotificaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

/**
 * Config class to set up necessary components for watching the MongoDB change
 * stream.
 *
 * @author brandon
 */
//@Configuration
//@EnableReactiveMongoRepositories("com.smarttmt.preuba_reactivo")
//public class MongoReactiveApplication extends AbstractReactiveMongoConfiguration {
//
// 
//    @Override
//    public MongoClient reactiveMongoClient() {
//        return MongoClients.create("mongodb://172.17.0.3:27017/");
//    }
//
//    @Override
//    public String getDatabaseName() {
//        return "see_prueba";
//    }
//
//    @Override
//    public ReactiveMongoTemplate reactiveMongoTemplate() {
//        return new ReactiveMongoTemplate(reactiveMongoClient(), getDatabaseName());
//    }
//
//   
//    
//}
